//
//  FBMyProfileRequest.swift
//  PageOne
//
//  Created by tiennv on 11/11/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import FacebookCore
import FacebookLogin
import SwiftyJSON

struct FBMyProfileRequest: GraphRequestProtocol {
    typealias Response = FBMyProfileResponse
    let graphPath = "/me"
    let parameters: [String: Any]? = ["fields": "id, name, email, gender, birthday, picture"]
    let accessToken = AccessToken.current
    let httpMethod: GraphRequestHTTPMethod = .GET
    let apiVersion: GraphAPIVersion = .defaultVersion
}

struct FBMyProfileResponse: GraphResponseProtocol {
    let profile: MyProfileModel
    init(rawResponse: Any?) {
        if let rawResponse = rawResponse {
            let json = JSON(rawResponse)
            self.profile = (try? MyProfileModel.decodeJSON(json: json)) ?? MyProfileModel()
        } else {
            self.profile = MyProfileModel()
        }
    }
}
