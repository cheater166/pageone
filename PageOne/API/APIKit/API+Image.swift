//
//  API+Product.swift
//  PageOne
//
//  Created by tiennv on 11/12/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import APIKit
import SwiftyJSON


public struct POSTImageResponse: BaseModel {
    public let id: String
    public let path: String
    
    public static func decodeJSON(json: JSON) throws -> POSTImageResponse {
        return POSTImageResponse(id: try json.tryString(),
                                 path: try json.tryString())
    }
}

public extension API {
    
    public struct POSTImageRequest: APIRequest {
        public typealias Response = POSTImageResponse
        public let image: Data?
        public init(image: UIImage) {
            self.image = UIImagePNGRepresentation(image)
        }
        
        public var method: HTTPMethod { return .post }
        public var path: String { return "/tmp_upload" }
        public var bodyParameters: BodyParameters? {
            guard let image = self.image else {
                return nil
            }
            return MultipartFormDataBodyParameters(parts: [MultipartFormDataBodyParameters.Part(data: image, name: "file")])
        }
        public func response(from object: Any, urlResponse: HTTPURLResponse) throws -> POSTImageResponse {
            return try JSON(object).tryModel(POSTImageResponse.self)
        }
    }
    
}
