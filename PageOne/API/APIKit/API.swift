//
//  API.swift
//  PageOne
//
//  Created by tiennv on 11/12/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import APIKit
import SwiftyJSON
import Reachability
import FBSDKCoreKit


public protocol APIRequest: Request {
    
}

extension APIRequest {
    
    public var baseURL: URL {
        return API.baseURL
    }
    
    public var dataParser: DataParser {
        return JSONDataParser(readingOptions: [])
    }
    
    public var bodyParameters: BodyParameters? {
        guard let parameters = self.parameters as? [String: Any] else {
            return nil
        }
        return FormURLEncodedBodyParameters(formObject: parameters, encoding: .utf8)
    }
    
    public func intercept(object: Any, urlResponse: HTTPURLResponse) throws -> Any {
        guard (200..<300).contains(urlResponse.statusCode) else {
            let json = JSON(object)
            print(json)
            throw ResponseError.unacceptableStatusCode(urlResponse.statusCode)
        }
        return object
    }
}

extension APIRequest where Response == Any {
    public func response(from object: Any, urlResponse: HTTPURLResponse) throws -> Any {
        return object
    }
}

extension APIRequest where Response == JSON {
    public func response(from object: Any, urlResponse: HTTPURLResponse) throws -> JSON {
        return JSON(object)
    }
}

extension APIRequest where Response: BaseModel {
    public func response(from object: Any, urlResponse: HTTPURLResponse) throws -> Response {
        let json = JSON(object)
        let result = json["data"]
        return try result.tryModel(Response.self)
    }
}


public class API {

    private static var _baseURLInternal: URL {
        return URL(string:"https://pageone.vn")!
    }

    public static let baseURL: URL = _baseURLInternal

    public static let session: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = [
            "Access-Token": FBSDKAccessToken.current().tokenString,
            "Accept": "application/json,text/json,text/javascript",
            "Accept-Encoding": "deflate,gzip",
            "Content-Type": "multipart/form-data,application/json"
        ]
        configuration.httpShouldUsePipelining = true
        configuration.timeoutIntervalForRequest = 10
        return Session(adapter: URLSessionAdapter(configuration: configuration), callbackQueue: .main)
    }()

    public static let preloadingSession: Session = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.httpAdditionalHeaders = [
            "Access-Token": FBSDKAccessToken.current().tokenString,
            "Accept": "application/json,text/json,text/javascript",
            "Accept-Encoding": "deflate,gzip",
            "Content-Type": "multipart/form-data,application/json"
        ]
        configuration.httpShouldUsePipelining = true
        configuration.timeoutIntervalForRequest = 60
        return Session(adapter: URLSessionAdapter(configuration: configuration), callbackQueue: .dispatchQueue(DispatchQueue.global(qos: .background)))
    }()
    
    public static let reachability: Reachability = {
        guard let host = _baseURLInternal.host, let reachability = Reachability(hostname: host) else {
            fatalError("\(#function) - Failed to initialize Reachability object.")
        }
        return reachability
    }()

}

