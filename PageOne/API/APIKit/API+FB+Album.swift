//
//  API+FB+Album.swift
//  PageOne
//
//  Created by tiennv on 11/12/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import APIKit
import SwiftyJSON

public struct POSTAlbumResponse: BaseModel {
    public let id: String
    public static func decodeJSON(json: JSON) throws -> POSTAlbumResponse {
        return POSTAlbumResponse(id: try json["id"].tryString())
    }
}

public struct POSTAlbumImageResponse: BaseModel {
    public let id: String
    public static func decodeJSON(json: JSON) throws -> POSTAlbumImageResponse {
        return POSTAlbumImageResponse(id: try json["id"].tryString())
    }
}

public extension API {
    public struct POSTAlbumRequest: APIFB {
        public typealias Response = POSTAlbumResponse
        
        let name: String
        let message: String
        let pageToken : String
        public init(name: String, message: String, pageToken: String) {
            self.name = name
            self.message = message
            self.pageToken = pageToken
        }
        
        public var method: HTTPMethod { return .post }
        public var path: String { return "/me/albums" }
        public var parameters: Any? {
            return [
                "name": self.name,
                "message": self.message,
                "access_token": self.pageToken
            ]
        }
        
        public func response(from object: Any, urlResponse: HTTPURLResponse) throws -> POSTAlbumResponse {
            return try JSON(object).tryModel(POSTAlbumResponse.self)
        }
    }
    
    public struct POSTAlbumImageRequest: APIFB {
        public typealias Response = POSTAlbumImageResponse
        
        let albumId: String
        let imageUrl: String
        let pageToken : String
        init(albumId: String, imageUrl: String, pageToken: String) {
            self.albumId = albumId
            self.imageUrl = imageUrl
            self.pageToken = pageToken
        }
        
        public var method: HTTPMethod { return .post }
        public var path: String { return "/\(self.albumId)/photos" }
        public var parameters: Any? {
            return [
                "access_token": self.pageToken,
                "url": self.imageUrl
            ]
        }
        
        public func response(from object: Any, urlResponse: HTTPURLResponse) throws -> POSTAlbumImageResponse {
            return try JSON(object).tryModel(POSTAlbumImageResponse.self)
        }
        
        //        // Default is FormURLEncodedBodyParameters
        //        public var bodyParameters: BodyParameters? {
        //
        //            let tokenPart = MultipartFormDataBodyParameters.Part(
        //                data: self.pageToken.data(using: .utf8)!,
        //                name: "access_token")
        //
        //
        //
        //            guard let image = self.image else {
        //                return nil
        //            }
        //
        //            let imagePart = MultipartFormDataBodyParameters.Part(
        //                data: image,
        //                name: "source",
        //                fileName: "avatar.jpg")
        //
        //            return MultipartFormDataBodyParameters(parts: [tokenPart, imagePart])
        //        }
    }
}
