//
//  API+FB.swift
//  PageOne
//
//  Created by tiennv on 12/2/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import APIKit
import SwiftyJSON
import Reachability
import FBSDKCoreKit

public protocol APIFB: APIRequest {
    
}

extension APIFB {
    public var baseURL: URL { return  URL(string: "https://graph.facebook.com")! }
}
