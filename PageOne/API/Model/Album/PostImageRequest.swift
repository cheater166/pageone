////
////  PostImageRequest.swift
////  PageOne
////
////  Created by tiennv on 11/12/17.
////  Copyright © 2017 tiennv. All rights reserved.
////
//
//import Foundation
//import FacebookCore
//import SwiftyJSON
//import FBSDKCoreKit
//
//
//
//struct PostImageRequest: GraphRequestProtocol {
//    typealias Response = PostImageResponse
//
//    let accessToken = AccessToken.current
//    let httpMethod: GraphRequestHTTPMethod = .POST
//    let apiVersion: GraphAPIVersion = .defaultVersion
//    
//    let albumId: String
//    let image: UIImage
//    let pageToken : String
//    init(albumId: String, image: UIImage, pageToken: String) {
//        self.albumId = albumId
//        self.image = image
//        self.pageToken = pageToken
//    }
//    
//    public var parameters: [String : Any]? {
//        var params: [String: Any] = ["access_token": self.pageToken]
////        if let image = UIImageJPEGRepresentation(self.image, 0.8) {
////            params["source"] = image
////        }
//        params["url"] = "http://rightbrainkids.net/wp-content/uploads/2015/10/Albert-Einstein-1.jpg"
//        params["caption"] = "test"
//        return params
//    }
//    
//    var graphPath: String { return "/\(self.albumId)/photos" }
//}
//
//struct PostImageResponse: GraphResponseProtocol {
//    var photoId: String?
//    init(rawResponse: Any?) {
//        if let rawResponse = rawResponse {
//            let json = JSON(rawResponse)
//            self.photoId =  json["value"].string ?? ""
//        }
//    }
//}

