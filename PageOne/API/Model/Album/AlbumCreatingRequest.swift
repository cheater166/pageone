////
////  AlbumCreatingRequest.swift
////  PageOne
////
////  Created by tiennv on 11/12/17.
////  Copyright © 2017 tiennv. All rights reserved.
////
//
//
//import Foundation
//import FacebookCore
//
//import SwiftyJSON
//
//struct AlbumCreatingRequest: GraphRequestProtocol {
//    typealias Response = AlbumCreatingResponse
//    
//    let graphPath = "/me/albums"
//    let accessToken = AccessToken.current
//    let httpMethod: GraphRequestHTTPMethod = .POST
//    let apiVersion: GraphAPIVersion = .defaultVersion
//    
//    let name: String
//    let message: String
//    let pageToken : String
//    init(name: String, message: String, pageToken: String) {
//        self.name = name
//        self.message = message
//        self.pageToken = pageToken
//    }
//    
//    public var parameters: [String : Any]? {
//        return [
//            "name": self.name,
//            "message": self.message,
//            "access_token": self.pageToken
//        ]
//    }
//}
//
//struct AlbumCreatingResponse: GraphResponseProtocol {
//    var albumId: String?
//    init(rawResponse: Any?) {
//        if let rawResponse = rawResponse {
//            let json = JSON(rawResponse)
//            self.albumId =  json["id"].string ?? ""
//        }
//    }
//}

