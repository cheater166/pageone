//
//  UserInfo.swift
//  QRScanner
//
//  Created by tiennv on 11/10/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import SwiftyJSON
import RealmSwift
import Foundation
import FacebookCore
import FacebookLogin
import RxSwift
import RxCocoa

// Facebook profile
public class MyProfileModel: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var fbID: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var gender: String = ""
    @objc dynamic var picture: String = ""
    @objc dynamic var birthday: String = ""
    
    override public static func primaryKey() -> String? {
        return "fbID"
    }
    
    public static func decodeJSON(json: JSON) throws -> MyProfileModel {
        let profile = MyProfileModel()
        profile.name = json["name"].string ?? ""
        profile.fbID = try json["id"].tryString()
        profile.email = json["email"].string ?? ""
        profile.gender = json["gender"].string ?? ""
        profile.picture = json["picture"]["data"]["url"].string ?? ""
        profile.birthday = json["birthday"].string ?? ""
        return profile
    }
    
    convenience init(json: JSON) {
        let value: [String: Any] = [
            "name": json["name"].string ?? "",
            "fbID": json["id"].string ?? "",
            "email": json["email"].string ?? "",
            "gender": json["gender"].string ?? "",
            "picture": json["picture"]["data"]["url"].string ?? "",
            "birthday": json["birthday"].string ?? ""
        ]
        self.init(value: value)
    }
}


// MARK: User information
class UserInfo: NSObject {
    static let shared = UserInfo()
    
    fileprivate let _profile: Variable<MyProfileModel?> = Variable(nil)
    public var profile: MyProfileModel? { return self._profile.value }
    
    private override init() {
        super.init()
        let realm = RealmStore.localRealm
        if let profile = realm.objects(MyProfileModel.self).first {
            self._profile.value = profile
        }
    }
    
    public func update(_ profile: MyProfileModel) {
        self._profile.value = profile
        self.removeOldProfiles()
        // Store local
        do {
            let realm = RealmStore.localRealm
            try realm.write {
                realm.add(profile, update: true)
            }
        } catch {
            assertionFailure("\(error)")
        }
    }
    
    public func clear() {
        self._profile.value = nil
        self.removeOldProfiles()
    }
    
    private func removeOldProfiles() {
        do {
            let realm = RealmStore.localRealm
            let objects = realm.objects(MyProfileModel.self)
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            assertionFailure("\(error)")
        }
    }
}

extension Reactive where Base: UserInfo {
    var profileDriver: Driver<MyProfileModel?> {
        return self.base._profile.asDriver()
    }
}

// MARK: Login/Logout Facebook
extension UserInfo {
    class func logout() {
        let loginManager = LoginManager()
        loginManager.logOut()
        UserInfo.shared.clear()
    }
    
    class func login(isPublishPermissions: Bool = true, controller: UIViewController) {
        
        let profileRequest: ((LoginResult) -> Void) = { (loginResult) in
            switch loginResult {
            case let .failed(error):
                print("failed", error)
            case .cancelled:
                print("cancelled")
            case let .success(grantedPermissions, declinedPermissions, accessToken):
                AccessToken.current = accessToken
                print(grantedPermissions)
                print(declinedPermissions)
                let connection = GraphRequestConnection()
                connection.add(FBMyProfileRequest()) { (response, result) in
                    switch result {
                    case let .success(response):
                        UserInfo.shared.update(response.profile)
                    case let .failed(error):
                        print("Custom Graph Request Failed: \(error)")
                    }
                }
                connection.start()
            }
        }
        
        let loginManager = LoginManager()
        if isPublishPermissions {
            let publishPermissions: [PublishPermission] = [.publishActions, .publishPages, .managePages, .rsvpEvent]
            loginManager.logIn(publishPermissions: publishPermissions, viewController: controller, completion: profileRequest)
        } else {
            let readPermissions: [ReadPermission] = [.publicProfile, .email, .userFriends, .pagesShowList, .pagesManageCta]
            loginManager.logIn(readPermissions: readPermissions, viewController: controller, completion: profileRequest)
        }
    }
}
