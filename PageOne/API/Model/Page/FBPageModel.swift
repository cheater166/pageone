//
//	RootClass.swift
//  PageOne
//
//  Created by tiennv on 11/12/17.
//  Copyright © 2017 tiennv. All rights reserved.
//


import SwiftyJSON

public struct FBPageModel: BaseModel {
    
    let data: [FBPageData]
    
    public static func decodeJSON(json: JSON) throws -> FBPageModel {
        return FBPageModel(data: try json["data"].tryModelArray(FBPageData.self))
    }
}

public struct FBPageData: BaseModel {
    
    let id : String
    let accessToken : String
    let name : String?
    let picture : String?
    
    public static func decodeJSON(json: JSON) throws -> FBPageData {
        return FBPageData(id: try json["id"].tryString(),
                          accessToken: try json["access_token"].tryString(),
                          name: json["name"].string,
                          picture: json["picture"]["data"]["url"].string)
    }
}



//class FBPageModel: BaseModel {
//
//    var data : [FBPageData]?
//    var paging : FBPaging?
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        data <- map["data"]
//        paging <- map["paging"]
//    }
//}
//
//class FBPageData : BaseModel{
//
//    var accessToken : String?
//    var id : String?
//    var name : String?
//    var picture : String?
//
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        accessToken <- map["access_token"]
//        id <- map["id"]
//        name <- map["name"]
//        picture <- map["picture"]
//    }
//}
//
//class FBPaging: BaseModel {
//
//    var cursors : FBPageCursor?
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        cursors <- map["cursors"]
//    }
//}
//
//class FBPageCursor : BaseModel{
//
//    var after : String?
//    var before : String?
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        after <- map["after"]
//        before <- map["before"]
//    }
//
//}
//
//class FBPictureData : BaseModel{
//
//    var url : String?
//    var width : Int?
//    var heigth : Int?
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        url <- map["url"]
//        width <- map["width"]
//        heigth <- map["heigth"]
//    }
//}
//
//class FBPagePicture : BaseModel{
//
//    var data : FBPictureData?
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        data <- map["data"]
//    }
//}

