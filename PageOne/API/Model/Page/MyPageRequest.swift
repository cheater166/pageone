//
//  MyPageRequest.swift
//  PageOne
//
//  Created by HA CONG THUAN on 11/7/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import FacebookCore
import SwiftyJSON

struct MyPageRequest: GraphRequestProtocol {
    typealias Response = MyPageResponse
    
    let graphPath = "/me/accounts"
    let parameters: [String : Any]? = ["fields": "access_token, name, picture"]
    let accessToken = AccessToken.current
    let httpMethod: GraphRequestHTTPMethod = .GET
    let apiVersion: GraphAPIVersion = .defaultVersion
}

struct MyPageResponse: GraphResponseProtocol {
    let page: FBPageModel?
    init(rawResponse: Any?) {
        // Decode JSON from rawResponse into other properties here.
        if let rawResponse = rawResponse {
            self.page = JSON(rawResponse).model(FBPageModel.self)
        } else {
            self.page = nil
        }
    }
}
