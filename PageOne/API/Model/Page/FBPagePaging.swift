//
//	FBPagePaging.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FBPagePaging : Basemodel{

	var cursors : FBPageCursor?


	class func newInstance(map: Map) -> Mappable?{
		return FBPagePaging()
	}
	required init?(map: Map){}
	private override init(){}

}