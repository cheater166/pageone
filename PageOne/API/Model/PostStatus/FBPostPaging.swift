////
////    FBPostPaging.swift
////
////    Create by Ha Cong Thuan on 10/11/2017
////    Copyright © 2017 GMO. All rights reserved.
////    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport
//
//import Foundation 
//import ObjectMapper
//
//
//class FBPostPaging : BaseModel{
//
//    var cursors : FBPostCursor?
//    
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        cursors <- map["cursors"]
//    }
//    
//}

