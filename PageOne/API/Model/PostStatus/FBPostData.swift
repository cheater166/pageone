////
////    FBPostData.swift
////
////    Create by Ha Cong Thuan on 10/11/2017
////    Copyright © 2017 GMO. All rights reserved.
////    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport
//
//import Foundation 
//import ObjectMapper
//
//
//class FBPostData : BaseModel{
//
//    var fullPicture : String?
//    var id : String?
//    var link : String?
//    var message : String?
//    var name : String?
//    var picture : String?
//    var properties : [FBPostProperty]?
//    var shares : FBPostShare?
//    
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        fullPicture <- map["fullPicture"]
//        id <- map["id"]
//        link <- map["link"]
//        message <- map["message"]
//        name <- map["name"]
//        picture <- map["picture"]
//        properties <- map["properties"]
//        shares <- map["shares"]
//    }
//    
//}

