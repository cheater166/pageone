////
////    FBPostCursor.swift
////
////    Create by Ha Cong Thuan on 10/11/2017
////    Copyright © 2017 GMO. All rights reserved.
////    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport
//
//import Foundation 
//import ObjectMapper
//
//
//class FBPostCursor : BaseModel{
//
//    var after : String?
//    var before : String?
//
//    override func mapping(map: Map) {
//        super.mapping(map: map)
//        after <- map["after"]
//        before <- map["before"]
//    }
//}

