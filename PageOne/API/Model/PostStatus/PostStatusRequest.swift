////
////  PostStatusRequest.swift
////  PageOne
////
////  Created by HA CONG THUAN on 11/10/17.
////  Copyright © 2017 HA CONG THUAN. All rights reserved.
////
//
//import Foundation
//import FacebookCore
//import AlamofireObjectMapper
//import ObjectMapper
//
//struct PostStatusRequest: GraphRequestProtocol {
//    struct Response: GraphResponseProtocol {
//        var post = FBPostModel()
//        init(rawResponse: Any?) {
//            // Decode JSON from rawResponse into other properties here.
//            if let results = rawResponse as? [String: Any] {
//                let response =  Mapper<FBPostModel>().map(JSON: results)
//                post = response!
//                print("Custom Graph Request Failed: \(response)")
//            }
//        }
//    }
//    var graphPath = ""
//    var parameters: [String : Any]? = ["fields":"message,link,full_picture,name,shares,picture,properties"]
//    var accessToken = AccessToken.current
//    var httpMethod: GraphRequestHTTPMethod = .GET
//    var apiVersion: GraphAPIVersion = .defaultVersion
//}

