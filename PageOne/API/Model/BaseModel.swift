//
//  BaseModel.swift
//  PageOne
//
//  Created by tiennv on 12/5/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import SwiftyJSON

public enum BaseModelError: Error {
    case decodeJSONFailed
}

/**
 Base for all model
 */
public protocol BaseModel {
    static func decodeJSON(json: JSON) throws -> Self
}

/**
 An extension allows BaseModel linking with SwiftyJSON.
 */
public extension JSON {
    
    public func model<T: BaseModel>(_ klass: T.Type) -> T? {
        guard self.type == .dictionary, self.count > 0 else {
            return nil
        }
        do {
            return try klass.decodeJSON(json: self)
        } catch {
            debugPrint("\(klass).decodeJSON failed with error: \(error)")
            return nil
        }
    }
    
    public func tryModel<T: BaseModel>(_ klass: T.Type) throws -> T {
        guard self.type == .dictionary else {
            throw JSONError.invalidJSON(json: self)
        }
        guard self.count > 0 else {
            throw JSONError.blankDictTry(json: self)
        }
        return try klass.decodeJSON(json: self)
    }
    
    public func modelArray<T: BaseModel>(_ klass: T.Type) -> [T]? {
        guard let jsonArray = self.array else {
            return nil
        }
        return jsonArray.flatMap { $0.model(klass) }
    }
    
    public func modelArrayValue<T: BaseModel>(_ klass: T.Type) -> [T] {
        return self.arrayValue.flatMap { $0.model(klass) }
    }
    
    public func tryModelArray<T: BaseModel>(_ klass: T.Type) throws -> [T] {
        guard let jsonArray = self.array else {
            if let error = JSONError.error(from: self) {
                throw error
            } else {
                throw JSONError.invalidJSON(json: self)
            }
        }
        return try jsonArray.flatMap { try $0.tryModel(klass) }
    }
    
}
