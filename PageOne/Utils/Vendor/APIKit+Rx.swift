//
//  APIKit+Rx.swift
//  PageOne
//
//  Created by tiennv on 11/12/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import APIKit
import Result

extension Session: ReactiveCompatible {}
extension Reactive where Base: Session {
    
    public func send<T: Request>(request: T) -> Observable<T.Response> {
        return Observable.create { observer in
            let task = self.base.send(request) { result in
                switch result {
                case .success(let value):
                    observer.onNext(value)
                    observer.onCompleted()
                case .failure(let error):
                    print(error)
                    observer.onError(error)
                }
            }
            return Disposables.create {
                task?.cancel()
            }
        }
    }
    
    public func sendAsDriver<T: Request>(request: T) -> Driver<Result<T.Response, SessionTaskError>> {
        let observable = Observable<Result<T.Response, SessionTaskError>>.create { observer in
            let task = self.base.send(request) { result in
                observer.onNext(result)
                observer.onCompleted()
            }
            return Disposables.create {
                task?.cancel()
            }
        }
        return observable.asDriver { _ in
            fatalError("This should never happen, the source observable here will never return onError()")
        }
    }
    
}
