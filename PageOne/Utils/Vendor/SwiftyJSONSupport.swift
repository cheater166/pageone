//
//  SwiftyJSONSupport.swift
//  QRScanner
//
//  Created by tiennv on 11/10/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import SwiftyJSON


public enum JSONError: Error {
    case unsupportedType(json: JSON)
    case indexOutOfBounds(json: JSON)
    case wrongType(json: JSON)
    case blankDictTry(json: JSON)
    case notExist(json: JSON)
    case invalidJSON(json: JSON)
    
    public static func error(from json: JSON) -> JSONError? {
        if let error = json.error {
            switch error {
            case .unsupportedType:
                return JSONError.unsupportedType(json: json)
            case .indexOutOfBounds:
                return JSONError.indexOutOfBounds(json: json)
            case .wrongType:
                return JSONError.wrongType(json: json)
            case .notExist:
                return JSONError.notExist(json: json)
            case .invalidJSON:
                return JSONError.invalidJSON(json: json)
            default:
                return nil
            }
        } else {
            return nil
        }
    }
}

extension JSONError: CustomStringConvertible, CustomDebugStringConvertible {
    
    private static func errorDescriptionString(from json: JSON) -> String {
        if let error = json.error {
            return " - \(error.localizedDescription)"
        } else {
            return ""
        }
    }
    
    public var description: String {
        switch self {
        case let .unsupportedType(json):
            return "JSONError.unsupportedType\(JSONError.errorDescriptionString(from: json))"
        case let .indexOutOfBounds(json):
            return "JSONError.indexOutOfBounds\(JSONError.errorDescriptionString(from: json))"
        case let .wrongType(json):
            return "JSONError.wrongType\(JSONError.errorDescriptionString(from: json))"
        case .blankDictTry:
            return "JSONError.blankDictTry"
        case let .notExist(json):
            return "JSONError.notExist\(JSONError.errorDescriptionString(from: json))"
        case let .invalidJSON(json):
            return "JSONError.invalidJSON\(JSONError.errorDescriptionString(from: json))"
        }
    }
    
    public var debugDescription: String {
        return self.description
    }
}

// MARK: - `try` Extensions

public extension JSON {
    
    public func tryGet<T>(_ f: @autoclosure () -> T?) throws -> T {
        guard let value = f() else {
            if let error = JSONError.error(from: self) {
                throw error
            } else {
                throw JSONError.invalidJSON(json: self)
            }
        }
        return value
    }
    
    public func tryDictionary() throws -> [String : JSON] {
        return try self.tryGet(self.dictionary)
    }
    
    public func tryDictionaryObject() throws -> [String: Any] {
        return try self.tryGet(self.dictionaryObject)
    }
    
    public func tryArray() throws -> [JSON] {
        return try self.tryGet(self.array)
    }
    
    public func tryArrayObject() throws -> [Any] {
        return try self.tryGet(self.arrayObject)
    }
    
    public func tryBool() throws -> Bool {
        return try self.tryGet(self.bool)
    }
    
    public func tryString() throws -> String {
        return try self.tryGet(self.string)
    }
    
    public func tryNumber() throws -> NSNumber {
        return try self.tryGet(self.number)
    }
    
    public func tryURL() throws -> URL {
        return try self.tryGet(self.url)
    }
    
    public func tryDouble() throws -> Double {
        return try self.tryGet(self.double)
    }
    
    public func tryFloat() throws -> Float {
        return try self.tryGet(self.float)
    }
    
    public func tryInt() throws -> Int {
        return try self.tryGet(self.int)
    }
    
    public func tryUInt() throws -> UInt {
        return try self.tryGet(self.uInt)
    }
    
    public func tryInt64() throws -> Int64 {
        return try self.tryGet(self.int64)
    }
    
    public func tryUInt64() throws -> UInt64 {
        return try self.tryGet(self.uInt64)
    }
    
}

// MARK: - `as` Extensions

public extension JSON {
    
    public var asString: String? {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).description
        default:
            return nil
        }
    }
    
    public var asStringValue: String {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).description
        default:
            return ""
        }
    }
    
    public func tryAsString() throws -> String {
        return try self.tryGet(self.asString)
    }
    
    public var asInt: Int? {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).intValue
        default:
            return nil
        }
    }
    
    public var asIntValue: Int {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).intValue
        default:
            return 0
        }
    }
    
    public func tryAsInt() throws -> Int {
        return try self.tryGet(self.asInt)
    }
    
    public var asDouble: Double? {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).doubleValue
        default:
            return nil
        }
    }
    
    public var asDoubleValue: Double {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).doubleValue
        default:
            return 0
        }
    }
    
    public func tryAsDouble() throws -> Double {
        return try self.tryGet(self.asDouble)
    }
    
    public var asBool: Bool? {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).boolValue
        default:
            return nil
        }
    }
    
    public var asBoolValue: Bool {
        switch self.type {
        case .bool, .number, .string:
            return (self.object as AnyObject).boolValue
        default:
            return false
        }
    }
    
    public func tryAsBool() throws -> Bool {
        return try self.tryGet(self.asBool)
    }
    
}

// MARK: - other type Extensions

public extension JSON {
    
    public var unixTimeDate: Date? {
        if let timeInterval = self.double {
            return Date(timeIntervalSince1970: timeInterval)
        } else {
            return nil
        }
    }
    
    public func tryUnixTimeDate() throws -> Date {
        let timeInterval = try self.tryDouble()
        return Date(timeIntervalSince1970: timeInterval)
    }
    
    public var asUnixTimeDate: Date? {
        if let timeInterval = self.asDouble {
            return Date(timeIntervalSince1970: timeInterval)
        } else {
            return nil
        }
    }
    
    public func tryAsUnixTimeDate() throws -> Date {
        let timeInterval = try self.tryAsDouble()
        return Date(timeIntervalSince1970: timeInterval)
    }
}
