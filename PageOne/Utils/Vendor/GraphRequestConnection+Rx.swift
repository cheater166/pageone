//
//  GraphRequestConnection+Rx.swift
//  PageOne
//
//  Created by tiennv on 11/10/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import RxSwift
import RxCocoa
import FacebookCore
import FacebookLogin

extension GraphRequestConnection: ReactiveCompatible {}
extension Reactive where Base: GraphRequestConnection {
    
    public func send<T: GraphRequestProtocol>(request: T) -> Observable<T.Response> {
        return Observable.create { observer in
            self.base.add(request) { response, result in
                switch result {
                case .success(let response):
                    observer.onNext(response)
                    observer.onCompleted()
                case .failed(let error):
                    print (error)
                    observer.onError(error)
                }
            }
            self.base.start()
            return Disposables.create {
                self.base.cancel()
            }
        }
    }
    
    public func sendAsDriver<T: GraphRequestProtocol>(request: T) -> Driver<T.Response?> {
        let observable = Observable<T.Response?>.create { observer in
            self.base.add(request) { response, result in
                switch result {
                case .success(let response):
                    observer.onNext(response)
                case .failed(let error):
                    print(error.localizedDescription)
                    observer.onNext(nil)
                }
                observer.onCompleted()
            }
            self.base.start()
            return Disposables.create {
                self.base.cancel()
            }
        }
        return observable.asDriver { _ in
            fatalError("This should never happen, the source observable here will never return onError()")
        }
    }
    
}
