//
//  RealmStore.swift
//  QRScanner
//
//  Created by tiennv on 11/10/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import Foundation
import RealmSwift


public final class RealmStore {
    public static var localRealm: Realm {
        do {
            return try Realm()
        } catch {
            fatalError("\(error)")
        }
    }
}
