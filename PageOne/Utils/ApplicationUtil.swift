//
//  ApplicationUtil.swift
//  linenews
//
//  Created by tiennv on 11/12/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import UIKit


public class ApplicationUtil: NSObject {
    public static let bundleIdentifier: String = {
        guard let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String else {
            fatalError("\(#function) - Could not find 'CFBundleIdentifier' dictionary in Info.plist.")
        }
        return value
    }()
    
    public static let bundleVersion: String = {
        guard let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String else {
            fatalError("\(#function) - Could not find 'CFBundleVersion' dictionary in Info.plist.")
        }
        return value
    }()
    
    public static let appVersion: String = {
        guard let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            fatalError("\(#function) - Could not find 'CFBundleShortVersionString' dictionary in Info.plist.")
        }
        return value
    }()
    
    public static let appName: String = {
        guard let value = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String else {
            fatalError("\(#function) - Could not find 'CFBundleDisplayName' dictionary in Info.plist.")
        }
        return value
    }()
}
