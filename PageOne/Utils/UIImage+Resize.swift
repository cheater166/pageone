//
//  UIImage+Resize.swift
//  PageOne
//
//  Created by tiennv on 11/18/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import UIKit

extension UIImage {
    func compressTo(_ expectedSizeInMb:Float) -> UIImage? {
        let sizeInBytes = Int(expectedSizeInMb * 1024.0 * 1024.0)
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = UIImageJPEGRepresentation(self, compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData, data.count < sizeInBytes {
            return UIImage(data: data)
        }
        return nil
    }
}
