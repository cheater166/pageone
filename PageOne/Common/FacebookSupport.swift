////
////  FacebookManager.swift
////  PageOne
////
////  Created by HA CONG THUAN on 11/2/17.
////  Copyright © 2017 HA CONG THUAN. All rights reserved.
////
//
//import Foundation
//import FacebookCore
//import FacebookLogin
//
//class FacebookSupport: NSObject {
//    static let shared = FacebookSupport()
//    
//    class func logout() {
//        let loginManager = LoginManager()
//        loginManager.logOut()
//        UserInfo.shared.clear()
//    }
//    
//    class func login(isPublicPermissions: Bool = true, controller: UIViewController) {
//        let loginManager = LoginManager()
//        
//        let completion: ((LoginResult) -> Void) = { (loginResult) in
//            switch loginResult {
//            case .failed(let error):
//                print("failed", error)
//            case .cancelled:
//                print("cancelled")
//            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
//                AccessToken.current = accessToken
//                print(grantedPermissions)
//                print(declinedPermissions)
//                let connection = GraphRequestConnection()
//                connection.add(FBMyProfileRequest()) { (response, result) in
//                    switch result {
//                    case .success(let response):
//                        UserInfo.shared.update(response.profile)
//                    case .failed(let error):
//                        print("Custom Graph Request Failed: \(error)")
//                    }
//                }
//                connection.start()
//            }
//        }
//        
//        if isPublicPermissions {
//            let publicPermissions: [PublishPermission] = [.publishActions, .publishPages, .managePages, .rsvpEvent]
//            loginManager.logIn(publishPermissions: publicPermissions, viewController: controller, completion: completion)
//        } else {
//            let readPermissions: [ReadPermission] = [.publicProfile, .email, .userFriends, .pagesShowList, .pagesManageCta]
//            loginManager.logIn(readPermissions: readPermissions, viewController: controller, completion: completion)
//        }
//    }
//    
////    class func getPageList(successBlock: @escaping ( _ status:Bool, _ response:FBPageModel?) -> Void) {
////        let connection = GraphRequestConnection()
////        let pageRequest = MyPageRequest()
////        connection.add(pageRequest) { response, result in
////            switch result {
////            case .success(let response):
////                successBlock(true, response.page)
////            case .failed(let error):
////                successBlock(false, nil)
////                print("Custom Graph Request Failed: \(error)")
////            }
////        }
////        connection.start()
////    }
//    
//}
//
//
////func rxGetPageList() -> Driver<MyPageResponse?> {
////    let connection = GraphRequestConnection()
////    let request = MyPageRequest()
////    return connection.rx.sendAsDriver(request: request)
////}
//
////func rxRequestCreateAlbum(name: String, message: String, pageToken: String) -> Driver<AlbumCreatingResponse?> {
////    let connection = GraphRequestConnection()
////    let request = AlbumCreatingRequest(name: name, message: message, pageToken: pageToken)
////    return connection.rx.sendAsDriver(request: request)
////}
////
////func rxRequestPost(albumId: String, image: UIImage, pageToken: String) -> Driver<PostImageResponse?> {
////    let connection = GraphRequestConnection()
////    let request = PostImageRequest(albumId: albumId, image: image, pageToken: pageToken)
////    return connection.rx.sendAsDriver(request: request)
////}
//
////1400085703619820,
////func getAllPost(pageID: String, successBlock: @escaping ( _ status:Bool, _ response:FBPostModel?) -> Void) {
////    let connection = GraphRequestConnection()
////    var pageRequest = PostStatusRequest()
////    pageRequest.graphPath = "/\(pageID)/feed"
////    connection.add(pageRequest) { response, result in
////        switch result {
////        case .success(let response):
////            successBlock(true, response.post)
////        case .failed(let error):
////            successBlock(false, nil)
////            print("Custom Graph Request Failed: \(error)")
////        }
////    }
////    connection.start()
////}
////
////func postNewStatus(status: String?, pageID: String, successBlock: @escaping ( _ status:Bool, _ response:FBPostModel?) -> Void) {
////    let connection = GraphRequestConnection()
////    var pageRequest = PostNewStatusRequest()
////    pageRequest.graphPath = "/\(pageID)/feed"
////    pageRequest.parameters = ["message":"Test new mesage","access_token":"EAACEdEose0cBALx7qvXZA3ZBqQqZCj6M12oQN9gZAyn9tob7L9HiAhUAMAyoDiNP1N057KYT2P7GVCGZAt05ZCAWWZAjp4CyumPulsYikhPmfvFEhNdgr5oi7qZAmFiRpSAY9aEugbd20roc8lzDIqVEe9QBxbfx9A5nosxA358C8VWRUkWssQhCY4TqjwOFRwuIjx15jz2Gz97F29GmV9wZB"]
////    connection.add(pageRequest) { response, result in
////        switch result {
////        case .success(let response):
////            successBlock(true, nil)
////        case .failed(let error):
////            successBlock(false, nil)
////            print("Custom Graph Request Failed: \(error)")
////        }
////    }
////    connection.start()
////}
//
