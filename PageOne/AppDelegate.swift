//
//  AppDelegate.swift
//  PageOne
//
//  Created by HA CONG THUAN on 10/27/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKCoreKit
import SlideMenuControllerSwift
import IQKeyboardManagerSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.createMenuView()
        IQKeyboardManager.sharedManager().enable = true
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }

}

extension AppDelegate {
    func createMenuView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let menuViewController = menuStoryboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let mNvc = UINavigationController(rootViewController: mainViewController)
        menuViewController.mainNavigationController = mNvc
        let slideMenuController = SlideMenuController(mainViewController: mNvc, leftMenuViewController: menuViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        if let appdelegate = UIApplication.shared.delegate as? AppDelegate {
            UIView.transition(with: appdelegate.window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                appdelegate.window?.rootViewController = slideMenuController
            }, completion: { completed in
                // maybe do something here
            })
            appdelegate.window?.makeKeyAndVisible()
        }
    }
}


