//
//  DatePickerViewController.swift
//  PageOne
//
//  Created by HA CONG THUAN on 11/5/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import UIKit

protocol CustomPickerViewDelegate {
    func pickerChangeValue(value: String, timeType: Bool)
}

class CustomDatePickerViewController: UIViewController {
    
    @IBOutlet weak var pickerView: UIDatePicker!
    var customPickerDelegate: CustomPickerViewDelegate?
    var timePickerType: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if timePickerType {
            pickerView.datePickerMode = .time
        } else {
            pickerView.datePickerMode = .date
        }
    }
    @IBAction func datePickerValueChange(_ sender: UIDatePicker) {
        var dateValue = ""
        let formatter = DateFormatter()
        if timePickerType {
            formatter.dateFormat = "h:mm a"
        } else {
            formatter.dateFormat = "dd/MM/yyyy"
        }
        dateValue = formatter.string(from: sender.date)
        customPickerDelegate?.pickerChangeValue(value: dateValue, timeType: timePickerType)
    }
    
    @IBAction func cancelPickerView(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func choosedItem(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
}
