//
//  MainViewController.swift
//  PageOne
//
//  Created by HA CONG THUAN on 10/30/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import FacebookCore
import FBSDKCoreKit

class MainViewController: BaseViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    fileprivate var loadingDisposable: Disposable? {
        didSet {
            self.showLoading(self.loadingDisposable != nil)
        }
    }
    
    var dataPageList: FBPageModel? {
        didSet {
            mainTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initMenuButton()
        
        let driver = GraphRequestConnection().rx.sendAsDriver(request: MyPageRequest())
        self.loadingDisposable = driver.drive(
            onNext: { [weak self] result in
                if let `self` = self, let pageModel = result?.page {
                    self.self.dataPageList = pageModel
                }
            },
            onCompleted: { [weak self] in
                if let `self` = self {
                    self.loadingDisposable = nil
                }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataPageList?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomMainTableViewCell") as! CustomMainTableViewCell
        cell.fillDataToCellWithData(data: (dataPageList?.data[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
