//
//  CustomMainTableViewCell.swift
//  PageOne
//
//  Created by HA CONG THUAN on 11/11/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CustomMainTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var avaImageView: UIImageView!
    
    func fillDataToCellWithData(data: FBPageData) {
        name.text = data.name
        if let picture = data.picture, let url = URL(string: picture) {
            self.avaImageView.kf.setImage(with: url)
        }
    }
}
