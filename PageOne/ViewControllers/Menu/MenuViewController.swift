//
//  MenuViewController.swift
//  PageOne
//
//  Created by HA CONG THUAN on 10/30/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import UIKit
import FacebookLogin
import FacebookCore
import Kingfisher
import RxSwift
import RxCocoa

enum AppMenuKey {
    
    case status
    case album
    case multipost
    case viewComment
    case managerInbox
    case managerTimeLine
    
    init?(row: Int) {
        switch (row) {
        case 0:
            self = .status
        case 1:
            self = .album
        case 2:
            self = .multipost
        case 3:
            self = .viewComment
        case 4:
            self = .managerInbox
        case 5:
            self = .managerTimeLine
        default:
            return nil
        }
    }
    
}

class MenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var loginFacebook: UIButton!
    let mainContents = ["Status", "Đăng album ảnh", "Đăng nhiều bài lên 1 page", "Xem comment đã xử lý", "Quản lý inbox", "Quản lý timeline"]
    let menu_icons = [ "ic_postnews", "ic_enterprise_regis", "ic_enterprise_login", "ic_newcontact", "ic_notification", "ic_enterprise_edit"]
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var frameAvatarImageView: UIImageView!
    
    var mainNavigationController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemsTableView.rowHeight = UITableViewAutomaticDimension
        itemsTableView.estimatedRowHeight = 200
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.width/2
        
        // Rx Data Binding, with scan result
        UserInfo.shared.rx.profileDriver.drive(onNext: { [weak self] profile in
            guard let `self` = self else { return }
            // Check login FB
            if let profile = UserInfo.shared.profile, let _ = AccessToken.current {
                self.nameLabel.text = profile.name
                self.loginFacebook.setTitle("Đăng Xuất", for: .normal)
                if let url = URL(string: profile.picture) {
                    self.avatarImageView.kf.setImage(with: url)
                }
            } else {
                self.nameLabel.text = ""
                self.loginFacebook.setTitle("Đăng nhập", for: .normal)
                self.avatarImageView.kf.cancelDownloadTask()
                self.avatarImageView.image = nil
            }
        }).disposed(by: self.disposeBag)
    }
    
    @IBAction func loginButton_Tap(_ sender: Any) {
        if let _ = UserInfo.shared.profile {
            UserInfo.logout()
        } else {
            UserInfo.login(controller: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as? MenuTableViewCell {
            cell.menuTitle.text = self.mainContents[indexPath.row]
            cell.menuIcon.image = UIImage(named: self.menu_icons[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let menu = AppMenuKey(row: indexPath.row) else { return }
        self.changeViewController(menu: menu)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func changeViewController(menu: AppMenuKey) {
        guard let mainNav = mainNavigationController else { return }
        switch menu {
        case .status:
            showScreen(mainNav, "Status", "StatusViewController")
        case .album:
            showScreen(mainNav, "Album", "AlbumViewController")
        case .multipost:
            showScreen(mainNav, "Multipost", "MultipostViewController")
        case .viewComment:
            showScreen(mainNav, "ViewComment", "ViewCommentViewController")
        case .managerInbox:
            showScreen(mainNav, "ManagerInbox", "ManagerInboxViewController")
        case .managerTimeLine:
            showScreen(mainNav, "ManagerTimeline", "ManagerTimelineViewController")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    private func showScreen( _ mainNav: UINavigationController, _ storyboardName: String, _ storyboardId: String) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardId)
        closeLeft()
        mainNav.pushViewController(viewController, animated: true)
    }
}

