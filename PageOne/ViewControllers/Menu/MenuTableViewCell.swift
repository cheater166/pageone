//
//  MenuTableViewCell.swift
//  PageOne
//
//  Created by HA CONG THUAN on 10/30/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuTitle: UILabel!
    
    func setupMenuItem(iconNane: String!, itemTitle: String!) {
        menuIcon.image = UIImage(named: iconNane)
        menuTitle.text = itemTitle
    }
    
}
