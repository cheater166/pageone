//
//  AlbumViewController.swift
//  PageOne
//
//  Created by tiennv on 11/2/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import RxSwift
import RxCocoa
import ActionSheetPicker_3_0
import FacebookCore
import FBSDKCoreKit

class AlbumViewController: BaseViewController {
    
    @IBOutlet weak var btnCreateAlbum: UIButton!
    @IBOutlet weak var tvContent: UITextView!
    @IBOutlet weak var tfAlbumName: UITextField!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnPickPage: UIButton!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    
    fileprivate typealias PostImage = (image: UIImage, url: String?)
    
    // MARK: private properties
    fileprivate var loadingDisposable: Disposable? {
        didSet {
            self.showLoading(self.loadingDisposable != nil || self.postImageDisposable != nil)
        }
    }
    fileprivate var postImageDisposable: Disposable? {
        didSet {
            self.showLoading(self.loadingDisposable != nil || self.postImageDisposable != nil)
        }
    }
    
    // Page data
    fileprivate var fbPageList: [FBPageData] = [] {
        didSet {
            self.btnPickPage.isUserInteractionEnabled = !self.fbPageList.isEmpty
            self.selectedPage = self.fbPageList.first
        }
    }
    fileprivate var selectedPage: FBPageData? {
        didSet {
            let name: String = self.selectedPage?.name ?? ""
            self.btnPickPage.setTitle(name, for: .normal)
        }
    }
    
    // Image set
    fileprivate var imageSet: [PostImage] = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                if self.imageSet.isEmpty {
                    self.collectionHeight.constant = 0
                } else {
                    let rows = (self.imageSet.count - 1)/Int(self.numberOfImageInRow) + 1
                    self.collectionHeight.constant = CGFloat(rows)*(self.sizeForImageCell.height + 2*self.collectionInsets.bottom)
                }
            }
        }
    }
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Đăng album ảnh"
        self.initBackButton()
        self.requestPageList()
        
        // Init collection view
        self.collectionView.backgroundColor = .white
        self.collectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        self.collectionHeight.constant = 0
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    // MARK: Actions
    @IBAction func createAlbum(_ sender: UIButton) {
        if let message = self.validateData() {
            self.showMessage(message)
        } else {
            self.requestCreatingAlbum()
        }
    }
    
    @IBAction func pickPhoto(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let galleryPicker = UIImagePickerController()
            galleryPicker.sourceType = .photoLibrary
            galleryPicker.delegate = self
            self.present(galleryPicker, animated: true)
        } else {
            self.showMessage("Không có quyền truy cập ảnh")
        }
    }
    
    @IBAction func takePhotoFromCamera(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let galleryPicker = UIImagePickerController()
            galleryPicker.sourceType = .camera
            galleryPicker.delegate = self
            self.present(galleryPicker, animated: true)
        } else {
            self.showMessage("Không có quyền truy cập camera")
        }
    }
    
    @IBAction func pickPage(_ sender: UIButton) {
        guard !self.fbPageList.isEmpty else {
            return
        }
        ActionSheetStringPicker.show(withTitle: "Chọn Page", rows: self.fbPageList.map{$0.name ?? ""}, initialSelection: 0, doneBlock: { [weak self] _, index, _ in
            self?.selectedPage = self?.fbPageList[index]
            }, cancel: nil, origin: sender)
    }
    
}

// MARK: request
extension AlbumViewController {
    func requestPageList() {
        guard AccessToken.current != nil else {
            self.showMessage("Vui lòng đăng nhập")
            return
        }
        guard self.loadingDisposable == nil else {
            return
        }
        
        let driver = GraphRequestConnection().rx.sendAsDriver(request: MyPageRequest())
        let disposable = driver.drive(
            onNext: { [weak self] result in
                if let `self` = self, let pageModel = result?.page {
                    self.fbPageList = pageModel.data
                }
            },
            onCompleted: { [weak self] in
                guard let `self` = self else { return }
                self.loadingDisposable = nil
        })
        self.loadingDisposable = disposable
    }
    
    func requestCreatingAlbum() {
        guard self.loadingDisposable == nil else {
            return
        }
        guard let token = self.selectedPage?.accessToken, let name = self.tfAlbumName.text, let message = self.tvContent.text else {
            return
        }
        
        let request = API.POSTAlbumRequest(name: name, message: message, pageToken: token)
        let driver = API.session.rx.sendAsDriver(request: request)
        let disposable = driver.drive(
            onNext: { [weak self] result in
                switch result {
                case let .success(value):
                    guard let `self` = self else { return }
                    self.requestPostImage(value.id)
                case .failure:
                    guard let `self` = self else { return }
                    self.showMessage("Không tạo được album")
                }
            },
            onCompleted: { [weak self] in
                guard let `self` = self else { return }
                self.loadingDisposable = nil
        })
        self.loadingDisposable = disposable
    }
    
    func requestPostImage(_ albumId: String) {
        guard self.postImageDisposable == nil else {
            return
        }
        guard let token = self.selectedPage?.accessToken, !self.imageSet.isEmpty else {
            return
        }
        
        // Post image to PAGEONE server
        let postImageFileObservables: [Observable<POSTImageResponse>] = self.imageSet.flatMap { [weak self] image in
            if let url = image.url, !url.isEmpty {
                // Emit `element` immediately
                return Observable.just(POSTImageResponse(id: "", path:url))
            }
            let request = API.POSTImageRequest(image: image.image)
            return Observable.create { observer in
                let task = API.session.rx.base.send(request) { result in
                    switch result {
                    case .success(let value):
                        if let `self` = self {
                            self.imageSet = self.imageSet.map{($0.image == image.image) ? PostImage(image.image, value.path) : $0}
                        }
                        observer.onNext(value)
                    case .failure(let error):
                        print(error)
                        // Emit `empty element` instead of error signal
                        // To ensure obseverables sequence is not interupted
                        observer.onNext(POSTImageResponse(id: "", path: ""))
                    }
                    observer.onCompleted()
                }
                return Disposables.create {
                    task?.cancel()
                }
            }
        }
        
        // Post image URL to FB Album
        let postImageToFBObservables = Observable.merge(postImageFileObservables).flatMap { (response: POSTImageResponse) -> Observable<POSTAlbumImageResponse> in
            if (response.path.isEmpty) {
                // Emit `empty element` immediately
                return Observable.just(POSTAlbumImageResponse(id: ""))
            }
            let request = API.POSTAlbumImageRequest(albumId: albumId, imageUrl: response.path, pageToken: token)
            return Observable.create { observer in
                let task = API.session.rx.base.send(request) { [weak self]result in
                    switch result {
                    case .success(let value):
                        if let `self` = self {
                            self.imageSet = self.imageSet.filter{$0.url != response.path}
                        }
                        observer.onNext(value)
                    case .failure(let error):
                        print(error)
                        // Emit `empty element` instead of error signal
                        // To ensure obseverables sequence is not interupted
                        observer.onNext(POSTAlbumImageResponse(id: ""))
                    }
                    observer.onCompleted()
                }
                return Disposables.create {
                    task?.cancel()
                }
            }
        }
        // Subcrible
        let driver = postImageToFBObservables.asDriver { error in
            return Driver.just(POSTAlbumImageResponse(id: ""))
        }
        let disposable = driver.drive( onCompleted: { [weak self] in
            guard let `self` = self else { return }
            self.postImageDisposable = nil
            if self.imageSet.isEmpty {
                self.showMessage("Đăng ảnh thành công!")
            } else {
                self.showMessage("Có lỗi xảy ra! Không đăng được ảnh!")
            }
        })
        self.postImageDisposable = disposable
    }
}

extension AlbumViewController {
    fileprivate func validateData() -> String? {
        guard let _ = self.selectedPage else {
            return "Chưa có page nào được chọn!"
        }
        guard let name = self.tfAlbumName.text, !name.isEmpty else {
            return "Chưa nhập tên Album!"
        }
        guard let content = self.tvContent.text, !content.isEmpty else {
            return "Chưa nhập nội dung Album!"
        }
        return nil
    }
    
    fileprivate func showMessage(_ message: String) {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension AlbumViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let compress = image.compressTo(0.5) {
            self.imageSet += [PostImage(compress, nil)]
        }
        self.dismiss(animated: true)
    }
}

// MARK: - UICollectionViewDataSource
extension AlbumViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageSet.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCollectionViewCell
        cell.pickedImage = self.imageSet[indexPath.row].image
        cell.onButtonCallback = { [weak self] image in
            if let `self` = self {
                self.imageSet = self.imageSet.filter{$0.image != image}
            }
        }
        return cell
    }
}

// MARK: - UICollectionViewDataSource
extension AlbumViewController {
    fileprivate var numberOfImageInRow: CGFloat {
        return 3.0
    }
    
    fileprivate var collectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    }
    
    fileprivate var sizeForImageCell: CGSize {
        let screenSize = self.view.bounds.size
        let size: CGFloat = screenSize.width/self.numberOfImageInRow - 2*self.collectionInsets.left
        return CGSize(width: size, height: size)
    }
}

extension AlbumViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.sizeForImageCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return self.collectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.collectionInsets.top
    }
}

