//
//  ImageCollectionViewCell.swift
//  PageOne
//
//  Created by tiennv on 11/11/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    private lazy var pickedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        self.contentView.addSubview(imageView)
        
        // Auto layout
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5).isActive = true
        imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -5).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5).isActive = true
        return imageView
    }()
    
    /// The button for load more or error. Shown in the closed state.
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "ic_delete"), for: .normal)
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(onButton), for: .touchUpInside)
        self.contentView.addSubview(button)
        // Auto layout
        button.translatesAutoresizingMaskIntoConstraints = false
        button.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0).isActive = true
        button.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
        button.widthAnchor.constraint(equalToConstant: 25).isActive = true
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        return button
    }()
    
    var pickedImage: UIImage? {
        didSet {
            self.pickedImageView.image = self.pickedImage
            self.button.isHidden = false
        }
    }
    
    override func prepareForReuse() {
        self.pickedImageView.image = nil
    }
    
    // MARK: - init/deinit
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInitialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInitialize()
    }
    
    private func commonInitialize() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.selectedBackgroundView = nil
    }
    
    // MARK: Action
    var onButtonCallback: ((UIImage) -> Void)?
    @objc private func onButton() {
        if let image = self.pickedImage {
            self.onButtonCallback?(image)
        }
    }
}
