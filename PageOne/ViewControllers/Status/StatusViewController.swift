//
//  StatusViewController.swift
//  PageOne
//
//  Created by HA CONG THUAN on 11/2/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import Foundation
import UIKit

class StatusViewController: BaseViewController, CustomPickerViewDelegate {
    
    @IBOutlet weak var timePost: UILabel!
    @IBOutlet weak var datePost: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Status/Photo/Video"
        initBackButton()
        
        
//        getAllPost(pageID: "670796976439274") { (succes, post) in
//            
//        }
//        postNewStatus(status: "New Status", pageID: "1400085703619820", successBlock: { (success, model) in
//            
//        })
    }
    
    @IBAction func timePost_Tap(_ sender: Any) {
        showPickerDateWithTimeType(type: true)
    }
    
    @IBAction func datePost_Tap(_ sender: Any) {
        showPickerDateWithTimeType(type: false)
    }
    
    func showPickerDateWithTimeType(type: Bool) {
        let storyboard = UIStoryboard(name: "Custom", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CustomDatePickerViewController") as! CustomDatePickerViewController
        vc.timePickerType = type
        vc.customPickerDelegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func pickerChangeValue(value: String, timeType: Bool) {
        if timeType {
            timePost.text = value
        } else {
           datePost.text = value
        }
    }
}
