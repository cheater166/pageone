//
//  Alerts.swift
//  QRScanner
//
//  Created by tiennv on 10/31/17.
//  Copyright © 2017 tiennv. All rights reserved.
//

import UIKit
import APIKit

class Alerts {
    
    enum AlertResult {
        case cancel
        case reload
    }
    
    @discardableResult
    static func showRequestFailedAlert(in viewController: UIViewController, error: SessionTaskError, message: String = "Có lỗi xảy ra.", callback: @escaping ((AlertResult) -> Void)) -> UIAlertController {
        let failureMessage: String
        switch error {
        case .connectionError:
            failureMessage = "Kiểm tra kết nối mạng."
        case .requestError, .responseError:
            failureMessage = message
        }
        let cancelButton = UIAlertAction(title: "Bỏ qua", style: .cancel) { _ in callback(.cancel) }
        let otherButton = UIAlertAction(title: "Thử lại", style: .default) { _ in callback(.reload) }
        
        let alertController = UIAlertController(title: nil, message: failureMessage, preferredStyle: .alert)
        alertController.addAction(cancelButton)
        alertController.addAction(otherButton)
        viewController.present(alertController, animated: true, completion: nil)
        return alertController
    }
    
}
