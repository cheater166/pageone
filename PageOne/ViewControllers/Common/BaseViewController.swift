//
//  ViewController.swift
//  PageOne
//
//  Created by HA CONG THUAN on 10/27/17.
//  Copyright © 2017 HA CONG THUAN. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SlideMenuControllerSwift
import SVProgressHUD

public typealias VoidBlock = (() -> Void)

class BaseViewController: UIViewController {
    
    enum ViewState {
        case disappeared
        case willAppear
        case willDisappear
        case appeared
    }
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.rx.notification(.UIApplicationWillEnterForeground)
            .map { _ in return true }
            .bind(to: self._isInForegroundVariable)
            .disposed(by: self.disposeBag)
        NotificationCenter.default.rx.notification(.UIApplicationDidEnterBackground)
            .map { _ in return false }
            .bind(to: self._isInForegroundVariable)
            .disposed(by: self.disposeBag)
        self._isInForegroundVariable.value = (UIApplication.shared.applicationState != .background)
        
        // Set up navigation
        if let navigationController = self.navigationController {
            navigationController.navigationBar.barTintColor = UIColor(hex6: 0x2F87EB)
            navigationController.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 40)]
            navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self._viewStateVariable.value = .willAppear
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self._viewStateVariable.value = .appeared
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self._viewStateVariable.value = .willDisappear
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self._viewStateVariable.value = .disappeared
    }
    
    // MARK: - Methods
    let disposeBag: DisposeBag = DisposeBag()
    
    var viewState: ViewState {
        return self._viewStateVariable.value
    }
    fileprivate let _viewStateVariable: Variable<ViewState> = Variable(.disappeared)
    
    var isViewAppeared: Bool {
        switch (self.viewState, self._isInForegroundVariable.value) {
        case (.appeared, true):
            return true
        default:
            return false
        }
    }
    fileprivate let _isInForegroundVariable: Variable<Bool> = Variable(true)
}

extension Reactive where Base: BaseViewController {
    var isViewAppeared: Driver<Bool> {
        let viewState: Observable<Void> = self.base._viewStateVariable.asObservable()
            .distinctUntilChanged()
            .map { _ in }
        let isInForeground: Observable<Void> = self.base._isInForegroundVariable.asObservable()
            .distinctUntilChanged()
            .map { _ in }
        return Observable.of(viewState, isInForeground)
            .merge()
            .map { [weak viewController = self.base] in
                if let viewController = viewController {
                    return viewController.isViewAppeared
                } else {
                    return false
                }
            }
            .asDriver(onErrorJustReturn: false)
            .distinctUntilChanged()
    }
}


extension BaseViewController {
    func initMenuButton() {
        let customButton = UIButton(frame: CGRect(x: 0, y: 10, width: 25, height: 25))
        customButton.setBackgroundImage(UIImage(named: "Menu"), for: .normal)
        customButton.addTarget(self, action: #selector(self.btnMenu_tapped), for: .touchUpInside)
        let leftButton = UIBarButtonItem(customView: customButton)
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    func initBackButton() {
        let customButton = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 28))
        customButton.setBackgroundImage(UIImage(named: "back_button"), for: .normal)
        customButton.addTarget(self, action: #selector(self.backButton_Onclick), for: .touchUpInside)
        let leftButton = UIBarButtonItem(customView: customButton)
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func backButton_Onclick() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func btnMenu_tapped() {
        if let slideMenuController = self.slideMenuController() {
            SlideMenuOptions.contentViewScale = 1
            slideMenuController.toggleLeft()
        }
    }
    
    func showLoading(_ isShow: Bool) {
        isShow ? SVProgressHUD.show() : SVProgressHUD.dismiss()
        self.view.isUserInteractionEnabled = !isShow
    }
}

